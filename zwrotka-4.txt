To tylko on, czytelnik
Który nie spostrzegłby powyrywanych stron
Ślepy na postęp, to tylko on, oni
Ci, którzy są zdominowani negatywnymi myślami
Wierzą, że świat kręci się dla nich
Pozwolili, by latami odpływały ambicje i plany
A cztery ściany stemperowały temperamenty
Biorąc fikcje za autentyk, kiedyś to wilki
Dziś po prostu sępy